package com.buba.yka.core;

import com.buba.yka.common.exception.BusinessException;
import com.buba.yka.common.result.ResponseEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

/**
 * @ClassName:ExceptionTest
 * @Auther: YooAo
 * @Description:
 * @Date: 2023/7/20 14:54
 * @Version: v1.0
 */
@Slf4j
public class ExceptionTest {

    @Test
    public void test(){
        try {
            System.out.println(1/0);
        } catch (Exception e) {
            throw new BusinessException(ResponseEnum.BORROW_AMOUNT_NULL_ERROR);//出错返回给前端下面代码不会执行
        }
        System.out.println(1111111);
    }

    @Test
    public void test2(){
        try {
            System.out.println(1/0);
        } catch (Exception e) {
            log.error("e",e);//出错打印日志下面代码会执行
        }
        System.out.println(222222222);
    }

    @Test
    public void test3(){
        try {

            System.out.println(1/0);
        } catch (Exception e) {
            e.printStackTrace();//出错报错下面代码会执行
        }
        System.out.println(222222222);
    }

    @Test
    public void test4(){
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
    }
}
