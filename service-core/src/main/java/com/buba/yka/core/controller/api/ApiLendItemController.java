package com.buba.yka.core.controller.api;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.result.R;
import com.buba.yka.core.hfb.RequestHelper;
import com.buba.yka.core.pojo.entity.BorrowInfo;
import com.buba.yka.core.pojo.entity.LendItem;
import com.buba.yka.core.pojo.vo.InvestVO;
import com.buba.yka.core.service.LendItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的出借记录表，记录投资人投资信息的 前端控制器
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "标的的投资")
@RestController
@RequestMapping("/api/core/lendItem")
@Slf4j
public class ApiLendItemController {

    @Resource
    LendItemService lendItemService;

    /**
     * 投资人对标的进行投资，尚融宝请求汇付宝携带着固定参数（汇付宝要求的）
     * 尚融宝：
     * 新增lend_item表记录（标的出借记录表，记录投资人投资信息的）与汇付宝的user_invest表进行绑定同一个投资编号订单号
     * 汇付宝：
     * 新增user_invest表记录（用户投资表），与尚融宝的lend_item表进行绑定同一个投资编号订单号
     * 更改user_account投资人账户表
     *  通过当前投资人的bind_code更改user_account投资人账户表账户金额和冻结金额字段（投资成功后账户金额-投资金额，冻结金额+投资金额）
     * @param investVO 投资信息对象（标的id，投资金额，投资人id，投资人姓名））
     * @param request 获取到投资人id和姓名
     * @return 给前端返回一个动态html，表单自动提交，生成一个动态表单的字符串，表单携带着固定参数：表单自动提交对汇付宝发送请求
     */
    @ApiOperation("会员投资提交数据")
    @PostMapping("/auth/commitInvest")
    public R commitInvest(@RequestBody InvestVO investVO, HttpServletRequest request) {

        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);//获取投资人id
        String userName = JwtUtils.getUserName(token);//获取投资人姓名
        investVO.setInvestUserId(userId);//设置
        investVO.setInvestName(userName);//设置

        //构建投资自动提交表单
        String formStr = lendItemService.commitInvest(investVO);

        return R.ok().data("formStr", formStr);
    }


    /**
     * 异步回调，与汇付宝数据同步，汇付宝向尚融宝发送请求，并携带固定参数
     * 目的：
     * 1、修改账户余额(user_account)，从账户余额中减去投资金额，在冻结金额中增加投资金额（投资成功后先把投资的钱冻结，该标的满标后(借款金额攒够时)，冻结解除）
     * 2、修改投资记录的状态为已支付（lend_item表status字段）（0：默认 1：已支付 2：已还款）
     * 3、修改标的记录(lend表)：投资人数(invest_num字段)，已投金额(invest_amount字段)
     * 4、增加交易流水(trans_flow表)
     * @param request 获取汇付宝向尚融宝发送的参数
     * @return 尚融宝对汇付包返回success，返回success汇付包就不会发送重试了（不会报错了）成功必须返回success
     */
    @ApiOperation("会员投资异步回调")
    @PostMapping("/notify")
    public String notify(HttpServletRequest request) {

        Map<String, Object> paramMap = RequestHelper.switchMap(request.getParameterMap());
        log.info("用户投资异步回调：" + JSON.toJSONString(paramMap));

        //校验签名 P2pInvestNotifyVo
        if(RequestHelper.isSignEquals(paramMap)) {
            if("0001".equals(paramMap.get("resultCode"))) {
                lendItemService.notify(paramMap);
            } else {
                log.info("用户投资异步回调失败：" + JSON.toJSONString(paramMap));
                return "fail";
            }
        } else {
            log.info("用户投资异步回调签名错误：" + JSON.toJSONString(paramMap));
            return "fail";
        }
        return "success";
    }

    /**
     * 在网站端标的详情展示投资列表
     * @param lendId
     * @return
     */
    @ApiOperation("获取投资列表")
    @GetMapping("/list/{lendId}")
    public R list(
            @ApiParam(value = "标的id", required = true)
            @PathVariable Long lendId) {
        List<LendItem> list = lendItemService.selectByLendId(lendId);
        return R.ok().data("list", list);
    }

    /**
     * 在网站端当前用户主页面，可查看的投资列表
     * @param page
     * @param limit
     * @return
     */
    @ApiOperation("借款申请信息列表分页")
    @GetMapping("/auth/list/{page}/{limit}")
    public R listPage(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,
            HttpServletRequest request
    ) {
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        Page<LendItem> pageParam = new Page<>(page, limit);

        Page<LendItem> list = lendItemService.selectListPage(pageParam,userId);

        return R.ok().data("list", list);
    }

}

