package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.IntegralGrade;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 积分等级表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface IntegralGradeMapper extends BaseMapper<IntegralGrade> {

}
