package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.LendItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 标的出借记录表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface LendItemMapper extends BaseMapper<LendItem> {

}
