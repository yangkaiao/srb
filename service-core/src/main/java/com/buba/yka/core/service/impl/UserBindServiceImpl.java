package com.buba.yka.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.core.enums.UserBindEnum;
import com.buba.yka.core.hfb.FormHelper;
import com.buba.yka.core.hfb.HfbConst;
import com.buba.yka.core.hfb.RequestHelper;
import com.buba.yka.core.mapper.UserBindMapper;
import com.buba.yka.core.pojo.entity.UserBind;
import com.buba.yka.core.pojo.entity.UserInfo;
import com.buba.yka.core.pojo.vo.UserBindVO;
import com.buba.yka.core.service.UserBindService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.buba.yka.core.service.UserInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户绑定表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class UserBindServiceImpl extends ServiceImpl<UserBindMapper, UserBind> implements UserBindService {


    @Resource
    private UserInfoService userInfoService;

    /**
     * 账号绑定，开户汇付包，账户绑定提交到托管平台的数据，携带着参数，表单的形式自动提交
     * 尚融宝新增新增了user_bind记录，汇付宝创建绑定账号（db_hfb数据库新建user_bind和user_account记录）生成bind_code字段
     * @param userBindVO 绑定的表单数据
     * @param userId 当前登录的token
     * @return
     */
    @Override
    public String commitBindUser(UserBindVO userBindVO, Long userId) {

        //不同的user_id，相同的身份证，如果存在，则不允许，不同的用户不能绑定同一个身份证
        //查询身份证号码是否绑定
        LambdaQueryWrapper<UserBind> userBindLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //SELECT * FROM user_bind WHERE is_deleted=0 AND (id_card = ? AND user_id != ?)
        userBindLambdaQueryWrapper.eq(UserBind::getIdCard,userBindVO.getIdCard())
                .ne(UserBind::getUserId,userId);//ne：不满足条件，查询不是uesrId的字段
        UserBind userBind1 = baseMapper.selectOne(userBindLambdaQueryWrapper);
        //USER_BIND_IDCARD_EXIST_ERROR(-301, "身份证号码已绑定"),
        Assert.isNull(userBind1, ResponseEnum.USER_BIND_IDCARD_EXIST_ERROR);


        //查询用户绑定信息
        userBindLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userBindLambdaQueryWrapper.eq(UserBind::getUserId,userId);
        UserBind userBind = baseMapper.selectOne(userBindLambdaQueryWrapper);

        //判断是否有绑定记录
        if(userBind == null){
            //如果没查询到说明当前用户没有绑定，所以创建用户绑定记录
            userBind = new UserBind();
            // BeanUtils会使用反射机制获取两个对象的属性列表,找出属性名相同的属性对。然后将userBindVO对象中相同名称属性的值赋值给userBind对应的属性。
            BeanUtils.copyProperties(userBindVO, userBind);//对象拷贝
            userBind.setUserId(userId);//用户id
            userBind.setStatus(UserBindEnum.NO_BIND.getStatus());//绑定状态
            baseMapper.insert(userBind);
        }else {
            //如果查询到了说明当前用户绑定过了，所以要更改绑定信息
            //曾经跳转到托管平台，但是未操作完成，此时将用户最新填写的数据同步到userBind对象
            //相同的user_id，相同的身份证，如果存在，那么就取出数据，做更新
            BeanUtils.copyProperties(userBindVO, userBind);//对象拷贝
            baseMapper.updateById(userBind);
        }


        //组装自动提交表单的参数
        //绑定账户,尚融宝对汇付包发请求需要携带的参数，详细看汇付包文档：3.3.3.绑定账户请求
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("agentId", HfbConst.AGENT_ID);//给商户分配的唯一标识
        paramMap.put("agentUserId", userId);//用户id
        paramMap.put("idCard",userBindVO.getIdCard());//身份证号
        paramMap.put("personalName", userBindVO.getName());//真实姓名
        paramMap.put("bankType", userBindVO.getBankType());//银行类型
        paramMap.put("bankNo", userBindVO.getBankNo());//银行卡号
        paramMap.put("mobile", userBindVO.getMobile());//手机号
        paramMap.put("returnUrl", HfbConst.USERBIND_RETURN_URL);//用户绑定同步回调同步返回到user界面
        paramMap.put("notifyUrl", HfbConst.USERBIND_NOTIFY_URL);///用户绑定异步回调交给尚融宝处理，给尚融宝发送该请求
        paramMap.put("timestamp", RequestHelper.getTimestamp());//时间戳
        paramMap.put("sign", RequestHelper.getSign(paramMap));//获取加密后的签名，有固定的格式加密，可以进去看看

        //构建充值自动提交表单，用表单形式提交到汇付包，表单带有自动提交
        // HfbConst.USERBIND_URL：提交地址，访问汇付包的接口
        //paramMap：携带的参数
        //formStr：生成了一个动态的表单
        String formStr = FormHelper.buildForm(HfbConst.USERBIND_URL, paramMap);
        return formStr;
    }

    /**
     * 账户绑定成功异步回调，与汇付宝数据同步，汇付包对尚融宝发送的该请求
     * 对尚融宝数据库进行修改绑定状态（user_info,user_bind）
     * 尚融宝user_bind表更新bind_code字段、status字段
     * 尚融宝user_info表更新 bind_code字段、name字段、idCard字段、bind_status字段
     * 尚融宝和汇付包通过（尚融宝user_info表的bind_code字段和user_bind表bind_code字段）
     * 和（汇付包的user_bind表中bind_code字段和user_account表中user_code字段）进行统一绑定，绑定成功
     * @param paramMap 汇付包对尚融宝发送请求携带的参数
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void notifyUserBind(Map<String, Object> paramMap) {

        String agentUserId = (String)paramMap.get("agentUserId");//用户id
        String bindCode = (String) paramMap.get("bindCode");//绑定账户协议号

        //尚融宝user_bind表更新bind_code字段、status字段
        //通过user_id查询user_bian表中数据进行更改bind_code字段、status字段
        LambdaUpdateWrapper<UserBind> userBindLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        userBindLambdaUpdateWrapper.eq(UserBind::getUserId,agentUserId)
                .set(UserBind::getBindCode,bindCode)
                .set(UserBind::getStatus,UserBindEnum.BIND_OK.getStatus());
        baseMapper.update(null,userBindLambdaUpdateWrapper);

        //通过user_id查询user_bind表中数据
        LambdaQueryWrapper<UserBind> userBindLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userBindLambdaQueryWrapper.eq(UserBind::getUserId,agentUserId);
        UserBind userBind = baseMapper.selectOne(userBindLambdaQueryWrapper);

        //尚融宝user_info表更新 bind_code字段、name字段、idCard字段、bind_status字段
        LambdaUpdateWrapper<UserInfo> UserInfoLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        UserInfoLambdaUpdateWrapper.eq(UserInfo::getId,agentUserId)
                .set(UserInfo::getBindCode,bindCode)
                .set(UserInfo::getBindStatus,UserBindEnum.BIND_OK.getStatus())
                .set(UserInfo::getName,userBind.getName())
                .set(UserInfo::getIdCard,userBind.getIdCard());
        userInfoService.update(null,UserInfoLambdaUpdateWrapper);

    }

    /**
     * 通过用户id获取投资人的绑定协议号
     * @param investUserId
     * @return
     */
    @Override
    public String getBindCodeByUserId(Long investUserId) {

        LambdaQueryWrapper<UserBind> userBindLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userBindLambdaQueryWrapper.eq(UserBind::getUserId,investUserId);
        UserBind userBind = baseMapper.selectOne(userBindLambdaQueryWrapper);

        return userBind.getBindCode();
    }
}
