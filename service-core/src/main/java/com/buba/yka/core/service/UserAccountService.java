package com.buba.yka.core.service;

import com.buba.yka.core.pojo.entity.UserAccount;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 用户账户 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface UserAccountService extends IService<UserAccount> {

    //生成动态html表单字符串，对汇付宝发送请求，携带固定的参数
    String commitCharge(BigDecimal chargeAmt, Long userId);

    //（1）尚融宝账户金额更改（user_account）（2）尚融宝添加交易流水（trans_flow）
    String notify(Map<String, Object> paramMap);

    //通过用户id查询当前投资人账户余额
    BigDecimal getAccount(Long userId);

    //用户提现，尚融宝调用汇付宝并携带固定参数
    String commitWithdraw(BigDecimal fetchAmt, Long userId);

    //用户提现异步回调，汇付宝请求尚融宝接口并携带参数，把数据同步到尚融宝
    void notifyWithdraw(Map<String, Object> paramMap);
}
