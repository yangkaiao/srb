package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.Lend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 标的准备表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface LendMapper extends BaseMapper<Lend> {

}
