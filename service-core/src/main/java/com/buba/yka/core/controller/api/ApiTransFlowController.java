package com.buba.yka.core.controller.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.result.R;
import com.buba.yka.core.pojo.entity.LendItemReturn;
import com.buba.yka.core.pojo.entity.TransFlow;
import com.buba.yka.core.service.TransFlowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "资金记录")
@RestController
@RequestMapping("/api/core/transFlow")
@Slf4j
public class ApiTransFlowController {

    @Resource
    private TransFlowService transFlowService;

    /**
     * 查询当前用户的资金流水
     * @param request
     * @return
     */
    @ApiOperation("获取当前用户的资金流水列表")
    @GetMapping("/auth/list/{page}/{limit}")
    public R list(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,
            HttpServletRequest request) {
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        Page<TransFlow> page1 = new Page<>(page,limit);
        Page<TransFlow> list = transFlowService.selectByUserId(page1,userId);
        return R.ok().data("list", list);
    }
}