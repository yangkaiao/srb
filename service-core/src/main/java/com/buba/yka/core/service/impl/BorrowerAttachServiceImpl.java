package com.buba.yka.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.buba.yka.core.mapper.BorrowerAttachMapper;
import com.buba.yka.core.pojo.entity.BorrowerAttach;
import com.buba.yka.core.pojo.vo.BorrowerAttachVO;
import com.buba.yka.core.service.BorrowerAttachService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 借款人上传资源表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class BorrowerAttachServiceImpl extends ServiceImpl<BorrowerAttachMapper, BorrowerAttach> implements BorrowerAttachService {


    /**
     * 通过借款人id查询借款人附件图片，获取附件VO列表
     * @param id 借款人id
     * @return
     */
    @Override
    public List<BorrowerAttachVO> selectBorrowerAttachVOList(Long id) {

        LambdaQueryWrapper<BorrowerAttach> borrowerAttachLambdaQueryWrapper = new LambdaQueryWrapper<>();
        borrowerAttachLambdaQueryWrapper.eq(BorrowerAttach::getBorrowerId,id);

        List<BorrowerAttach> borrowerAttachList = baseMapper.selectList(borrowerAttachLambdaQueryWrapper);

        List<BorrowerAttachVO> borrowerAttachVOList = new ArrayList<>();

        borrowerAttachList.forEach(borrowerAttach -> {
            BorrowerAttachVO borrowerAttachVO = new BorrowerAttachVO();
            borrowerAttachVO.setImageType(borrowerAttach.getImageType());//图片类型（idCard1：身份证正面，idCard2：身份证反面，house：房产证，car：车）
            borrowerAttachVO.setImageUrl(borrowerAttach.getImageUrl());//图片路径（阿里云存储路径）

            borrowerAttachVOList.add(borrowerAttachVO);
        });

        return borrowerAttachVOList;
    }
}
