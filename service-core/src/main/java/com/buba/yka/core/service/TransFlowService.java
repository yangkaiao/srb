package com.buba.yka.core.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.pojo.bo.TransFlowBO;
import com.buba.yka.core.pojo.entity.TransFlow;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 交易流水表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface TransFlowService extends IService<TransFlow> {

    //尚融宝添加交易流水（trans_flow）
    void saveTransFlow(TransFlowBO transFlowBO);

    //判断流水号是否存在，解决当回调重试时，金额和流水会重复增加
    boolean isSaveTransFlow(String agentBillNo);

    //查询当前用户的资金流水
    Page<TransFlow> selectByUserId(Page<TransFlow> page,Long userId);
}
