package com.buba.yka.core.service.impl;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.core.enums.BorrowInfoStatusEnum;
import com.buba.yka.core.enums.BorrowerStatusEnum;
import com.buba.yka.core.enums.UserBindEnum;
import com.buba.yka.core.mapper.BorrowInfoMapper;
import com.buba.yka.core.mapper.BorrowerMapper;
import com.buba.yka.core.mapper.IntegralGradeMapper;
import com.buba.yka.core.mapper.UserInfoMapper;
import com.buba.yka.core.pojo.entity.BorrowInfo;
import com.buba.yka.core.pojo.entity.Borrower;
import com.buba.yka.core.pojo.entity.IntegralGrade;
import com.buba.yka.core.pojo.entity.UserInfo;
import com.buba.yka.core.pojo.vo.BorrowInfoApprovalVO;
import com.buba.yka.core.pojo.vo.BorrowerDetailVO;
import com.buba.yka.core.service.BorrowInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.buba.yka.core.service.BorrowerService;
import com.buba.yka.core.service.DictService;
import com.buba.yka.core.service.LendService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 借款信息表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class BorrowInfoServiceImpl extends ServiceImpl<BorrowInfoMapper, BorrowInfo> implements BorrowInfoService {

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private IntegralGradeMapper integralGradeMapper;

    @Resource
    private BorrowerMapper borrowerMapper;

    @Resource
    private BorrowerService borrowerService;

    @Resource
    private DictService dictService;

    @Resource
    private LendService lendService;


    /**
     * 用户界面业务：通过当前用户所获积分评估可借款额度，通过积分等级表评估
     * @param userId 当前登录用户id
     * @return
     */
    @Override
    public BigDecimal getBorrowAmount(Long userId) {
        //获取当前用户所获积分
        UserInfo userInfo = userInfoMapper.selectById(userId);
        //    LOGIN_MOBILE_ERROR(208, "用户不存在"),
        Assert.notNull(userInfo, ResponseEnum.LOGIN_MOBILE_ERROR);
        Integer integral = userInfo.getIntegral();

        //，通过积分等级表评估,根据积分查询借款额度， integral_start<integral<integral_end
        //le：小于等于，ge：大于等于
        QueryWrapper<IntegralGrade> queryWrapper = new QueryWrapper<>();
        queryWrapper.le("integral_start", integral);
        queryWrapper.ge("integral_end", integral);
        IntegralGrade integralGradeConfig = integralGradeMapper.selectOne(queryWrapper);
        if(integralGradeConfig == null){
            return new BigDecimal("0");
        }
        return integralGradeConfig.getBorrowAmount();
    }


    /**
     * 用户界面业务：提交借款申请，新增borrow_info表
     * 1、实现思路
     * 借款人提交借款要 判断借款人账户绑定状态 与 借款人信息审批状态，只有这两个状态都成立才能借款，这两个状态都在会员表中
     * 目标：将借款申请表单中用户填写的数据保存在borrow_info数据库表中
     * @param borrowInfo 表单数据
     * @param userId 用户id
     */
    @Override
    public void saveBorrowInfo(BorrowInfo borrowInfo, Long userId) {

        //获取userInfo信息
        UserInfo userInfo = userInfoMapper.selectById(userId);

        //判断当前用户是否绑定账号（汇付包）判断用户绑定状态
        Assert.isTrue(userInfo.getBindStatus().intValue() == UserBindEnum.BIND_OK.getStatus().intValue(),
                ResponseEnum.USER_NO_BIND_ERROR);

        //判断借款人信息审核状态是否通过
        Assert.isTrue(userInfo.getBorrowAuthStatus().intValue() == BorrowerStatusEnum.AUTH_OK.getStatus() ,
                ResponseEnum.USER_NO_AMOUNT_ERROR);

        //判断借款额度是否足够，判断前端传过来的额度是否小于当前用户所获额度
        //查询当前用户的借款额度，通过当前用户所获积分评估借款额度
        BigDecimal borrowAmount = this.getBorrowAmount(userId);
        Assert.isTrue(borrowInfo.getAmount().doubleValue() <= borrowAmount.doubleValue() ,ResponseEnum.USER_AMOUNT_LESS_ERROR);


        //设置用户id
        borrowInfo.setUserId(userId);
        //设置年化利，百分比转小数 年化利率乘以100（divide：除以），存储小数
        BigDecimal divide = borrowInfo.getBorrowYearRate().divide(new BigDecimal(100));
        borrowInfo.setBorrowYearRate(divide);
        //设置借款申请审核状态，认证中
        borrowInfo.setStatus(BorrowInfoStatusEnum.CHECK_RUN.getStatus());
        baseMapper.insert(borrowInfo);

    }


    /**
     * 用户界面业务：获取当前登录的借款人的借款申请审批状态
     * @param userId 用户id
     * @return 审批状态
     */
    @Override
    public Integer getStatusByUserId(Long userId) {

        LambdaQueryWrapper<BorrowInfo> borrowInfoLambdaQueryWrapper = new LambdaQueryWrapper<>();
        borrowInfoLambdaQueryWrapper.select(BorrowInfo::getStatus).eq(BorrowInfo::getUserId,userId);

        List<Object> objects = baseMapper.selectObjs(borrowInfoLambdaQueryWrapper);
        if(objects.size() == 0){
            //借款人尚未提交信息
            return BorrowInfoStatusEnum.NO_AUTH.getStatus();
        }

        Integer status = (Integer) objects.get(0);

        return status;
    }


    /**
     * 后台管理系统业务：借款申请信息表（borrow_info）进行自定义分页
     * 目的：在实体类中添加姓名，手机号和其它参数（存放数据字典的名称和审核状态）属性
     * 在把returnMethod和moneyUse字段的值通过数据字典显示名称（他们的值是数据字段的value值）展示在后台管理系统页面中
     * @param pageParam 分页数据
     * @param keyword 模糊查询
     * @return
     */
    @Override
    public Page<BorrowInfo> selectListPage(Page<BorrowInfo> pageParam,String keyword) {

        //通过两表连查，borrower表查出的姓名，手机号和BorrowInfo表的所有数据都赋值到BorrowInfo实体类中了
        Page<BorrowInfo> borrowInfoList = baseMapper.selectBorrowInfoList(pageParam,keyword);

        //getRecords()：分页里的全部数据，循环
        borrowInfoList.getRecords().forEach(borrowInfo -> {
            //设置其它参数(实体类中Param属性)，把returnMethod和moneyUse字段的值通过数据字典显示名称（他们的值是数据字段的value值）和审核状态名称，展示在后台管理系统页面中
            String returnMethod = dictService.getNameByParentDictCodeAndValue("returnMethod", borrowInfo.getReturnMethod());//设置还款方式
            String moneyUse = dictService.getNameByParentDictCodeAndValue("moneyUse", borrowInfo.getMoneyUse());//设置资金用途
            String status = BorrowInfoStatusEnum.getMsgByStatus(borrowInfo.getStatus());//通过状态号获取状态名称
            borrowInfo.getParam().put("returnMethod",returnMethod);
            borrowInfo.getParam().put("moneyUse",moneyUse);
            borrowInfo.getParam().put("status",status);
        });

        return borrowInfoList;
    }


    /**
     * 在网站端当前用户主页面，可查看的借款列表
     * @param pageParam
     * @return
     */
    @Override
    public Page<BorrowInfo> selectListPage(Page<BorrowInfo> pageParam,Long userId) {
        LambdaQueryWrapper<BorrowInfo> borrowInfoLambdaQueryWrapper = new LambdaQueryWrapper<>();
        borrowInfoLambdaQueryWrapper.eq(BorrowInfo::getUserId,userId);
        Page<BorrowInfo> borrowInfoPage = baseMapper.selectPage(pageParam, borrowInfoLambdaQueryWrapper);
        //getRecords()：分页里的全部数据，循环
        borrowInfoPage.getRecords().forEach(borrowInfo -> {
            //设置其它参数(实体类中Param属性)，把returnMethod和moneyUse字段的值通过数据字典显示名称（他们的值是数据字段的value值）和审核状态名称，展示在后台管理系统页面中
            String returnMethod = dictService.getNameByParentDictCodeAndValue("returnMethod", borrowInfo.getReturnMethod());//设置还款方式
            String moneyUse = dictService.getNameByParentDictCodeAndValue("moneyUse", borrowInfo.getMoneyUse());//设置资金用途
            String status = BorrowInfoStatusEnum.getMsgByStatus(borrowInfo.getStatus());//通过状态号获取状态名称
            borrowInfo.getParam().put("returnMethod",returnMethod);
            borrowInfo.getParam().put("moneyUse",moneyUse);
            borrowInfo.getParam().put("status",status);
        });

        return borrowInfoPage;
    }

    /**
     * 后台管理系统业务：通过借款申请id查询借款申请详细信息
     * 需求：借款详情展示借款信息与借款人信息
     * @param id 借款信息表id
     * @return
     */
    @Override
    public Map<String, Object> getBorrowInfoDetail(Long id) {

        //1、获取借款申请信息（borrow_info表）（BorrowInfo实体类）
        //通过借款申请id查询借款申请对象
        BorrowInfo borrowInfo = baseMapper.selectById(id);

        //组装数据：
        //设置其它参数(实体类中Param属性)，把returnMethod和moneyUse字段的值通过数据字典显示名称（他们的值是数据字段的value值）和审核状态名称，展示在后台管理系统页面中
        String returnMethod = dictService.getNameByParentDictCodeAndValue("returnMethod", borrowInfo.getReturnMethod());//设置还款方式
        String moneyUse = dictService.getNameByParentDictCodeAndValue("moneyUse", borrowInfo.getMoneyUse());//设置资金用途
        String status = BorrowInfoStatusEnum.getMsgByStatus(borrowInfo.getStatus());//通过状态号获取状态名称
        borrowInfo.getParam().put("returnMethod",returnMethod);
        borrowInfo.getParam().put("moneyUse",moneyUse);
        borrowInfo.getParam().put("status",status);

        //2、获取借款人信息（borrower表）（BorrowerDetailVO实体类）
        //通过borrow_info表中的userid查询borrower对象
        LambdaQueryWrapper<Borrower> borrowerLambdaQueryWrapper = new LambdaQueryWrapper<>();
        borrowerLambdaQueryWrapper.eq(Borrower::getUserId,borrowInfo.getUserId());
        Borrower borrower = borrowerMapper.selectOne(borrowerLambdaQueryWrapper);
        //通过借款人id，组装借款人对象
        BorrowerDetailVO borrowerDetailVO = borrowerService.getBorrowerDetailVOById(borrower.getId());

        //组装数据
        Map<String, Object> result = new HashMap<>();
        result.put("borrowInfo", borrowInfo);
        result.put("borrower", borrowerDetailVO);
        return result;
    }

    /**
     * 借款申请进行审批
     * @param borrowInfoApprovalVO  后端管理系统组装表单：接收借款申请审批表单对象数据
     *  目的：
     *  1、修改借款信息状态
     *  2、审核通过则创建标的
     * 需求：
     * 管理平台借款审批，审批通过后产生标的，审批前我们需要跟借款人进行电话沟通，确定借款年化和平台服务费率（平台收益），
     * 借款年化可能根据实际情况调高或调低；起息日通常我们把它确定为募集结束时间（或放款时间）
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void approval(BorrowInfoApprovalVO borrowInfoApprovalVO) {

        //1、修改借款申请审批状态（borrow_info）
        //获取进行审批的id
        Long id = borrowInfoApprovalVO.getId();
        //通过id获取借款申请对象
        BorrowInfo borrowInfo = baseMapper.selectById(id);
        borrowInfo.setStatus(borrowInfoApprovalVO.getStatus());
        baseMapper.updateById(borrowInfo);

        //2、 如果借款申请审核通过了，则创建标的（lend）
        if(borrowInfo.getStatus().intValue() == BorrowInfoStatusEnum.CHECK_OK.getStatus().intValue()){
            //TODO 创建新标的
            lendService.createLend(borrowInfoApprovalVO,borrowInfo);
        }
    }
}
