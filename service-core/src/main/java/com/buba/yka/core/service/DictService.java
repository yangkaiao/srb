package com.buba.yka.core.service;

import com.buba.yka.core.pojo.dto.ExcelDictDTO;
import com.buba.yka.core.pojo.entity.Dict;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 数据字典 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface DictService extends IService<Dict> {

    //excel导入
    void importData(InputStream inputStream);

    //数据导出excel
    List<ExcelDictDTO> listDictData();

    //根据上级id获取子节点数据列表
    List<Dict> listByParentId(Long parentId);

    //根据dictCode编码获取下级节点
    List<Dict> findByDictCode(String dictCode);

    //通过表中编码字段和value字段查询对应的名称
    String getNameByParentDictCodeAndValue(String dictCode, Integer value);
}
