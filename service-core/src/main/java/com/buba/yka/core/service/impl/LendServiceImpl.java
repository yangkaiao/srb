package com.buba.yka.core.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.common.exception.BusinessException;
import com.buba.yka.core.enums.LendStatusEnum;
import com.buba.yka.core.enums.ReturnMethodEnum;
import com.buba.yka.core.enums.TransTypeEnum;
import com.buba.yka.core.hfb.HfbConst;
import com.buba.yka.core.hfb.RequestHelper;
import com.buba.yka.core.mapper.*;
import com.buba.yka.core.pojo.bo.TransFlowBO;
import com.buba.yka.core.pojo.entity.*;
import com.buba.yka.core.pojo.vo.BorrowInfoApprovalVO;
import com.buba.yka.core.pojo.vo.BorrowerDetailVO;
import com.buba.yka.core.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.buba.yka.core.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.net.http.HttpClient;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 标的准备表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
@Slf4j
public class LendServiceImpl extends ServiceImpl<LendMapper, Lend> implements LendService {

    @Resource
    private DictService dictService;

    @Resource
    private BorrowerMapper borrowerMapper;

    @Resource
    private BorrowerService borrowerService;

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private UserAccountMapper userAccountMapper;

    @Resource
    private TransFlowService transFlowService;
    
    @Resource
    private LendItemService lendItemService;

    @Resource
    private LendReturnService lendReturnService;

    @Resource
    private LendItemReturnService lendItemReturnService;

    /**
     * 后端管理系统：如果借款申请审核通过了，则创建标的（lend）
     * @param borrowInfoApprovalVO 审批借款申请时，点击审批，接收表单数据，组装表单，审批对象
     * @param borrowInfo 借款申请信息数据
     */
    @Override
    public void createLend(BorrowInfoApprovalVO borrowInfoApprovalVO, BorrowInfo borrowInfo) {

        Lend lend = new Lend();
        lend.setUserId(borrowInfo.getUserId());//设置用户id
        lend.setBorrowInfoId(borrowInfo.getId());//设置借款申请id
        lend.setLendNo(LendNoUtils.getLendNo());//设置标的编码（随机生成的）
        lend.setTitle(borrowInfoApprovalVO.getTitle());//设置标的标题（前端传过来的标的名称）
        lend.setAmount(borrowInfo.getAmount());//设置标的金额（借款金额）
        lend.setPeriod(borrowInfo.getPeriod());//设置投资期限（借款期限，规定时间还款（如1个月，2个月））

        //设置年化利率（前端传过来的是百分比整数12%，保存到数据库小数，所以要除以一百 divide：除以）从审批对象中获取
        lend.setLendYearRate(borrowInfoApprovalVO.getLendYearRate().divide(new BigDecimal(100)));
        lend.setServiceRate(borrowInfoApprovalVO.getServiceRate().divide(new BigDecimal(100)));//设置平台服务费率跟年化利率一样

        lend.setReturnMethod(borrowInfo.getReturnMethod());//设置还款方式（存个数字就行）
        lend.setLowestAmount(new BigDecimal(100));//设置最低金额
        lend.setInvestAmount(new BigDecimal(0));//设置已投金额
        lend.setInvestNum(0);//设置投资人数
        lend.setPublishDate(LocalDateTime.now());//设置发布日期

        //设置开始日期（起息日期，审批对象中获取）把字符串类型的日期转为时间对象
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate lendStartDate = LocalDate.parse(borrowInfoApprovalVO.getLendStartDate(), dtf);
        lend.setLendStartDate(lendStartDate);

        //设置结束日期，开始日期LendStartDate + 投资期限Period（借款期限（如1个月，2个月））= 结束日期
        //plusMonths：月份相加
        LocalDate localDate = lendStartDate.plusMonths(borrowInfo.getPeriod());
        lend.setLendEndDate(localDate);

        lend.setLendInfo(borrowInfoApprovalVO.getLendInfo());//设置标的描述信息

        //设置平台预期收益
        //          每个月的平台服务费 = 一年的平台服务费率 / 12(1年12个月)
        //                                                                   除以12保留8位小数,ROUND_DOWN：剩余小数后面都直接舍弃相当于抹零
        BigDecimal monthRate = lend.getServiceRate().divide(new BigDecimal(12), 8, BigDecimal.ROUND_DOWN);
        //          平台预期收益 =  标的金额(借款额度) * 每个月的平台服务费 * 投资期限（借款期限，规定时间还款（如1个月，2个月））
        BigDecimal expectAmount = borrowInfo.getAmount().multiply(monthRate).multiply(new BigDecimal(borrowInfo.getPeriod()));
        lend.setExpectAmount(expectAmount);//设置平台预期收益

        lend.setRealAmount(new BigDecimal(0));//设置实际收益

        lend.setStatus(LendStatusEnum.INVEST_RUN.getStatus());//设置标的状态 INVEST_RUN(1, "募资中")


        lend.setCheckTime(LocalDateTime.now());//审核时间
        lend.setCheckAdminId(1L); //审核人

        baseMapper.insert(lend);
    }

    /**
     * 后台管理系统：标的列表分页查询
     * @param page1 分页数据
     * @return
     */
    @Override
    public Page<Lend> selectList(Page<Lend> page1) {

        Page<Lend> LendList = baseMapper.selectPage(page1, null);

        //循环getRecords()：所有的分页数据都在里面
        LendList.getRecords().forEach(lend -> {
            //设置Lend实体类里面的扩充属性param，把Lend表里的returnMethod(还款方式)和status(状态)整数改为名称放到map集合中
            String returnMethod = dictService.getNameByParentDictCodeAndValue("returnMethod", lend.getReturnMethod());
            String status = LendStatusEnum.getMsgByStatus(lend.getStatus());
            lend.getParam().put("returnMethod",returnMethod);
            lend.getParam().put("status",status);
        });


        return LendList;
    }


    /**
     * 获取标的详细信息
     * 目的：
     * 1、获取标的详细详细
     * 2、获取借款人详细信息
     * @param id 标的id
     * @return
     */
    @Override
    public Map<String, Object> getLendDetail(Long id) {

        //查询标的对象
        Lend lend = baseMapper.selectById(id);
        //组装数据
        //设置Lend实体类里面的扩充属性param，把Lend表里的returnMethod(还款方式)和status(状态)整数改为名称放到map集合中
        String returnMethod = dictService.getNameByParentDictCodeAndValue("returnMethod", lend.getReturnMethod());
        String status = LendStatusEnum.getMsgByStatus(lend.getStatus());
        lend.getParam().put("returnMethod", returnMethod);
        lend.getParam().put("status", status);

        //根据user_id获取借款人对象
        QueryWrapper<Borrower> borrowerQueryWrapper = new QueryWrapper<Borrower>();
        borrowerQueryWrapper.eq("user_id", lend.getUserId());
        Borrower borrower = borrowerMapper.selectOne(borrowerQueryWrapper);
        //组装借款人对象
        BorrowerDetailVO borrowerDetailVO = borrowerService.getBorrowerDetailVOById(borrower.getId());

        //组装数据
        Map<String, Object> result = new HashMap<>();
        result.put("lend", lend);
        result.put("borrower", borrowerDetailVO);

        return result;
    }

    /**
     * 计算投资所获收益，所获总利息
     * @param invest 投资金额
     * @param yearRate 年化收益
     * @param totalmonth 期数（几个月后还款）
     * @param returnMethod 还款方式 1-等额本息 2-等额本金 3-每月还息一次还本 4-一次还本
     * @return 所获收益，所获总利息
     */
    @Override
    public BigDecimal getInterestCount(BigDecimal invest, BigDecimal yearRate, Integer totalmonth, Integer returnMethod) {
        BigDecimal interestCount;
        //计算总利息，判断是哪一种还款方式，就用哪一种计算
        if (returnMethod.intValue() == ReturnMethodEnum.ONE.getMethod()) {
            interestCount = Amount1Helper.getInterestCount(invest, yearRate, totalmonth);
        } else if (returnMethod.intValue() == ReturnMethodEnum.TWO.getMethod()) {
            interestCount = Amount2Helper.getInterestCount(invest, yearRate, totalmonth);
        } else if(returnMethod.intValue() == ReturnMethodEnum.THREE.getMethod()) {
            interestCount = Amount3Helper.getInterestCount(invest, yearRate, totalmonth);
        } else {
            interestCount = Amount4Helper.getInterestCount(invest, yearRate, totalmonth);
        }
        return interestCount;
    }


    /**
     * 后台管理系统：对标的进行放款功能，对汇付宝发送同步远程调用（携带固定参数）
     * 放款是一个同步接口，直接远程调用发送post请求到汇付宝（携带着固定参数），直接响应固定数据(map集合)，没有通过from表单去动态发送请求，也不用异步回调
     * 标的募资时间到，平台会操作放款或撤标，如果达到放款条件则操作放款（并非满标才能放款，可以投资了多少随时放款）
     * step1：放款后汇付宝处理业务：
     * （1）修改user_account表，释放每一个与当前这个标的投标的投资人的冻结金额(freeze_amount)，
     *     把所有对当前标的投资的金额减去平台服务费转账到当前标的的借款人金额(amount)中。（并非满标才能放款，可以投资了多少随时放款）
     *     所以要通过已投金额去计算服务费，那么借款人就收到的金额就是 已投金额-服务费
     * （2）修改user_invest表状态status为1（状态（0：默认 1：已放款 -1：已撤标））
     * step2：汇付宝业务成功后，返回尚融宝放款成功，同步到尚融宝处理业务如下
     * （1）修改标的（lend表）状态（status(2, "还款中")）和 标的平台实际收益（real_amount）和标的放款时间（payment_time）
     * （2）给借款账号转入放款金额（已投金额-服务费）(user_account表) 借款人收到的放款金额就是 已投金额-服务费
     * （3）增加借款交易流水（trans_flow）
     * （4）解冻并扣除投资人资金,释放每一个与当前这个标的投标的投资人的冻结金额(freeze_amount)(user_account表)
     * （5）增加投资人交易流水（trans_flow）
     * （6）生成借款人还款计划（lend_return）和出借人回款计划（lend_item_return）
     * @param id 标的id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void makeLoan(Long id) {
        //获取标的信息
        Lend lend = baseMapper.selectById(id);

        //调用汇付宝放款借款
        //携带的参数：map集合
        HashMap<String, Object> map = new HashMap<>();
        map.put("agentId", HfbConst.AGENT_ID);//给商户分配的唯一标识
        map.put("agentProjectCode",lend.getLendNo());//放款项目(标的)编号。
        String agentBillNo = LendNoUtils.getLoanNo();
        map.put("agentBillNo", agentBillNo);//放款编号

        //并非满标才能放款，所以要通过已投金额去计算服务费，那么借款人就收到的金额就是 已投金额-服务费
        // 每个月的平台服务费 = 一年的平台服务费率(service_rate) / 12(1年12个月)
        //                                                  除以12保留8位小数,ROUND_DOWN：剩余小数后面都直接舍弃相当于抹零
        BigDecimal monthRate = lend.getServiceRate().divide(new BigDecimal(12), 8, BigDecimal.ROUND_DOWN);
        // 平台服务费 =  已投投资金额 * 每个月的平台服务费 * 投资期限（如1个月，2个月）
        BigDecimal realAmount = lend.getInvestAmount().multiply(monthRate).multiply(new BigDecimal(lend.getPeriod()));
        //放款后，把所有投资的金额减去对应的服务费才是借款人所获金额
        map.put("mchFee",realAmount);//商户手续费(平台实际收益)

        map.put("timestamp", RequestHelper.getTimestamp());//时间戳
        String sign = RequestHelper.getSign(map);
        map.put("sign", sign);//获取加密后的签名，有固定的格式加密，可以进去看看

        log.info("放款参数：" + JSONObject.toJSONString(map));

        //发送同步远程调用，map:请求参数，HfbConst.MAKE_LOAD_URL：请求地址，result：响应结果
        JSONObject result = RequestHelper.sendRequest(map, HfbConst.MAKE_LOAD_URL);
        log.info("放款结果：" + result.toJSONString());
        //放款结果：{"agentProjectCode":"LEND20230804194044277",标的编号
        // "voteAmt":"9958.3334000000",放款金额=已投金额-服务费
        // "hyFee":"0",
        // "mchFee":"41.6666000000", 平台服务费
        // "resultCode":"0000","resultMsg":"放款成功"}

        //放款失败
        if (!"0000".equals(result.getString("resultCode"))) {
            throw new BusinessException(result.getString("resultMsg"));
        }

//      step2：汇付宝业务成功后，返回尚融宝放款成功，同步到尚融宝处理业务如下
//      （1）修改标的（lend表）状态（status(2, "还款中")）和 标的平台实际收益（real_amount）和标的放款时间（payment_time）
            lend.setStatus(LendStatusEnum.PAY_RUN.getStatus());//PAY_RUN(2, "还款中"),
            //并非满标才能放款，所以要通过已投金额去计算服务费,就是平台实际收益
            //如：借款10000元，投资了9000元就放款了，就通过9000元去计算服务费，那么借款人就收到的金额就是 9000-服务费
            lend.setRealAmount(realAmount);//标的平台实际收益（real_amount）
            lend.setPaymentTime(LocalDateTime.now());//放款时间
            baseMapper.updateById(lend);

//      （2）给借款账号转入放款金额（已投金额-服务费）(user_account表)
            //获取借款人bind_coed
            Long userId = lend.getUserId();//标的的userid就是借款人
            UserInfo userInfo = userInfoMapper.selectById(userId);
            String bindCode = userInfo.getBindCode();

            //借款人收到的放款金额就是 已投金额-服务费
            BigDecimal total = new BigDecimal(result.getString("voteAmt"));//汇付宝处理好的结果，与汇付宝同步
            userAccountMapper.updateAccount(bindCode,total,new BigDecimal(0));

//      （3）增加借款交易流水（trans_flow）
            TransFlowBO transFlowBO = new TransFlowBO(
                    agentBillNo,//放款编号
                    bindCode,//借款人bind_code
                    total,//交易金额
                    TransTypeEnum.BORROW_BACK,//BORROW_BACK(5,"放款到账"),
                    "借款放款到账，编号：" + lend.getLendNo());//项目编号
            transFlowService.saveTransFlow(transFlowBO);

//      （4）解冻并扣除投资人资金,释放每一个与当前这个标的投标的投资人的冻结金额(freeze_amount)(user_account表)
            //通过标的id和状态为1已支付的查询lendItem表投资人信息
            List<LendItem> lendItemList = lendItemService.selectByLendId(id, 1);
            for (LendItem lendItem : lendItemList) {
                Long investUserId = lendItem.getInvestUserId();
                UserInfo userInfo1 = userInfoMapper.selectById(investUserId);
                String investBindCode = userInfo1.getBindCode();//投资人bind_code
                //投资人账号冻结金额转出
                BigDecimal investAmount = lendItem.getInvestAmount(); //投资金额
                //账户余额为0不变，冻结金额减去当前投资人投资的金额 .negate():变为负数
                userAccountMapper.updateAccount(investBindCode,new BigDecimal(0),investAmount.negate());

//      （5）增加投资人交易流水（trans_flow）
                TransFlowBO investTransFlowBO = new TransFlowBO(
                        LendNoUtils.getTransNo(),
                        investBindCode,
                        investAmount,
                        TransTypeEnum.INVEST_UNLOCK,//INVEST_UNLOCK(3,"放款解锁"),
                        "冻结资金转出，出借放款，编号：" + lend.getLendNo());//项目编号
                transFlowService.saveTransFlow(investTransFlowBO);
            }



//      （6）生成借款人还款计划和出借人回款计划
            this.repaymentPlan(lend);


    }


    /**
     * 还款计划
     * 标的期限三期 有十个投资人,还三期，每期的还款拆分成10份，
     * 从借款的第二个月开始还，还三个月，每个月都要对投资人还一期的钱，第三个月还的就是总金额-前两个月还的=剩下的金额
     * 通过标的还款期限，分别统计每个月应还所有投资人多少钱，分别插入每个月的数据到还款计划表
     * @param lend 标的
     */
    private void repaymentPlan(Lend lend) {

//        1、创建还款计划列表
//        根据还款期限生成还款计划 (for period)
//        {
//            创建还款计划对象
//            填充基本属性
//
//            判断是否是最后一期还款
//            设置还款状态
//
//            将还款对象加入还款计划列表
//        }
//        批量保存

        //创建还款计划列表
        ArrayList<LendReturn> lendReturnList = new ArrayList<>();

        // 根据还款期限生成还款计划 (for period)，有多少期限就生成多少个还款计划
        int len = lend.getPeriod().intValue();
        for (int i = 1; i <= len; i++) {
            //创建还款计划对象
            LendReturn lendReturn = new LendReturn();
            lendReturn.setLendId(lend.getId());//设置标的id
            lendReturn.setBorrowInfoId(lend.getBorrowInfoId());//设置借款信息id
            lendReturn.setReturnNo(LendNoUtils.getReturnNo());//"还款批次号
            lendReturn.setUserId(lend.getUserId());//设置借款人用户id
            lendReturn.setAmount(lend.getAmount());//设置借款金额
            lendReturn.setBaseAmount(lend.getInvestAmount());//设置计息本机额（已投金额）
            lendReturn.setLendYearRate(lend.getLendYearRate());//设置年化率
            lendReturn.setCurrentPeriod(i);//设置当前的期限
            lendReturn.setReturnMethod(lend.getReturnMethod());//设置还款方式

//           说明：还款计划中的这三项 = 回款计划中对应的这三项和：因此需要先生成对应的回款计划
//            lendReturn.setPrincipal()//设置本金
//            lendReturn.setInterest();//设置利息
//            lendReturn.setTotal()//设置本息（本金+利息）

            lendReturn.setFee(new BigDecimal(0));//设置手续费，在放款时借款用户已经交过了
            lendReturn.setReturnDate(lend.getLendStartDate().plusMonths(i));//设置还款时指定的还款日期，第二个月开始还款，plusMonths：月份相加
            lendReturn.setOverdue(false);//设置是否逾期


            //判断是否是最后一期还款，如果i==期限就是说明是最后一个月了
            if (i==len){
                lendReturn.setLast(true);//设置是否是最后一期还款
            }else {
                lendReturn.setLast(false);
            }

            lendReturn.setStatus(0);//设置状态（0-未归还 1-已归还）
            //将还款对象加入还款计划列表
            lendReturnList.add(lendReturn);
        }
        //批量保存
        lendReturnService.saveBatch(lendReturnList);

 /////////////////////////////////////////////////////////////////////////////////



        //生成期数和还款记录的id对应的键值对集合
        //循环lendReturnList，获取lendReturnList中每一个还款期数为键与还款计划id为值对应map，lendReturnMap{1:10,2:11,3:12}
        Map<Integer, Long> lendReturnMap = lendReturnList.stream().collect(
                Collectors.toMap(LendReturn::getCurrentPeriod, LendReturn::getId)
        );

 ///////////////////////////////////////////////////////////////////////////////////////////////////


//        第二步在回款方法中
//        3、创建所有投资的所有回款记录列表
//        获取当前标的下的所有的已支付的投资
//        遍历投资列表{
//            根据投资记录的id调用回款计划生成的方法，得到当前这笔投资的回款计划列表

//            将当前这笔投资的回款计划列表 放入 所有投资的所有回款记录列表
//        }

        //======================================================
        //=============获取所有投资者，生成回款计划===================
        //======================================================
        //所有投资人回款计划列表
        List<LendItemReturn> lendItemReturnAllList = new ArrayList<>();
        //获取投资成功的投资记录，对标的投资的所有投资人
        List<LendItem> lendItemList = lendItemService.selectByLendId(lend.getId(), 1);
        //通过对标的投资的所有投资人循环生成回款列表
        for (LendItem lendItem : lendItemList) {

            //创建回款计划列表,获取每一个投资人回款计划列表，根据投资记录的id调用回款计划生成的方法，得到当前这笔投资的回款计划列表
            List<LendItemReturn> lendItemReturnList = this.returnInvest(lendItem.getId(), lendReturnMap, lend);
            //组装所有投资人回款计划列表，将当前这笔投资的回款计划列表 放入 所有投资的所有回款记录列表
            lendItemReturnAllList.addAll(lendItemReturnList);
        }



//        4、遍历还款记录列表{
//
//            通过filter、map、reduce将相关期数的回款数据过滤出来
//            将当前期数的所有投资人的数据相加，就是当前期数的所有投资人的回款数据(本金、利息、总金额)
//
//            将计算出的数据填充入还款计划记录
//
//        }
//
//        批量更新还款计划列表


        //遍历还款记录列表
        //更新还款计划中的相关金额数据，统计每一个月对所有投资人应还的金额
        for (LendReturn lendReturn : lendReturnList) {

            //统计每一个月对所有投资人应还的本金,通过filter、map、reduce将相关期数的回款数据过滤出来
            BigDecimal sumPrincipal = lendItemReturnAllList.stream()
                    //过滤条件：当回款计划中的还款计划id == 当前还款计划id的时候，每一期回款计划对应这一个还款计划id
                    .filter(item -> item.getLendReturnId().longValue() == lendReturn.getId().longValue())
                    //将所有回款计划中计算的每月应收本金相加
                    .map(LendItemReturn::getPrincipal)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            //统计每一个月对所有投资人应还的利息
            BigDecimal sumInterest = lendItemReturnAllList.stream()
                    .filter(item -> item.getLendReturnId().longValue() == lendReturn.getId().longValue())
                    .map(LendItemReturn::getInterest)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            //统计每一个月对所有投资人应还的本息
            BigDecimal sumTotal = lendItemReturnAllList.stream()
                    .filter(item -> item.getLendReturnId().longValue() == lendReturn.getId().longValue())
                    .map(LendItemReturn::getTotal)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            lendReturn.setPrincipal(sumPrincipal); //每期还款本金
            lendReturn.setInterest(sumInterest); //每期还款利息
            lendReturn.setTotal(sumTotal); //每期还款本息
        }
        lendReturnService.updateBatchById(lendReturnList);


    }


    /**
     * 回款计划 （针对某一笔投资的回款计划，过还款期限统计出每一个投资人通每个月的回款计划）（期限如果有三个月，一个投资人就会插入三条记录分别是1 2 3月分别还多少钱）
     * 以下主要业务是：通过借款人的期限对当前投资人统计借款人每个月应还投资人多少钱，计算获取到本金+利息=本息..插入lend_item_return表，
     * 与lend_return表进行绑定，还款表1对多回款表
     * @param lendItemId 投资记录id
     * @param lendReturnMap 还款期数与还款计划id对应map，获取还款记录表中每一个还款期数为键与还款计划id为值对应map，lendReturnMap{1:10,2:11,3:12}
     * @param lend 标的对象
     * @return
     */
    public List<LendItemReturn> returnInvest(Long lendItemId, Map<Integer, Long> lendReturnMap, Lend lend) {

        //获取投资人记录信息
        LendItem lendItem = lendItemService.getById(lendItemId);

        //获取当前投资人投资金额
        BigDecimal amount = lendItem.getInvestAmount();

        //获取年化率
        BigDecimal yearRate = lend.getLendYearRate();

        //获取期限
        Integer totalMonth = lend.getPeriod();


        Map<Integer, BigDecimal> mapInterest = null;  //还款期数 -> 利息
        Map<Integer, BigDecimal> mapPrincipal = null; //还款期数 -> 本金
        //借钱12000，利息：0.12，期限：3个月，用的是第二种等额本金计算的
        //等额本金---每月本金:{1=4000.00000000, 2=4000.00000000, 3=4000.00000000}通过这个循环
        //等额本金---每月利息:{1=120.00, 2=80.00, 3=40.00}//或通过这个循环

        //根据还款方式计算本金和利息
        //1、等额本息方式计算，利息和本金
        if (lend.getReturnMethod().intValue() == ReturnMethodEnum.ONE.getMethod()) {
            //利息
            mapInterest = Amount1Helper.getPerMonthInterest(amount, yearRate, totalMonth);
            //本金
            mapPrincipal = Amount1Helper.getPerMonthPrincipal(amount, yearRate, totalMonth);
        }
        //2、等额本金方式计算，利息和本金
        else if (lend.getReturnMethod().intValue() == ReturnMethodEnum.TWO.getMethod()) {
            mapInterest = Amount2Helper.getPerMonthInterest(amount, yearRate, totalMonth);
            mapPrincipal = Amount2Helper.getPerMonthPrincipal(amount, yearRate, totalMonth);
        }
        //3、按月付息到期还本方式计算，利息和本金
        else if (lend.getReturnMethod().intValue() == ReturnMethodEnum.THREE.getMethod()) {
            mapInterest = Amount3Helper.getPerMonthInterest(amount, yearRate, totalMonth);
            mapPrincipal = Amount3Helper.getPerMonthPrincipal(amount, yearRate, totalMonth);
        }
        //4、一次还本还息方式计算，利息和本金
        else {
            mapInterest = Amount4Helper.getPerMonthInterest(amount, yearRate, totalMonth);
            mapPrincipal = Amount4Helper.getPerMonthPrincipal(amount, yearRate, totalMonth);
        }


//        2、创建回款计划列表
//        {
//            创建回款计划记录
//            循环
//            根据当前期数，获取还款计划的id将还款记录关联到回款记录
//            设置回款记录的基本属性计算回款本金、利息和总额(注意最后一个月的计算)
//            设置回款状态和是否逾期
//            将回款记录放入回款列表
//        }
//        批量保存


        //创建回款计划列表，以下业务是：对当前投资人统计借款人每个月应还多少钱，获取到本金+利息=本息，每一个月对应的还款id
        List<LendItemReturn> lendItemReturnList = new ArrayList<>();
        //循环利息或本金的map集合，以期限循环，因为循环次数都一样，每一个月的期数为键对应着本期该还的利息或该还的本金为值
        //借钱12000，利息：0.12，期限：3个月，用的是第二种等额本金计算的
        //等额本金---每月本金:{1=4000.00000000, 2=4000.00000000, 3=4000.00000000}通过这个循环
        //等额本金---每月利息:{1=120.00, 2=80.00, 3=40.00}//或通过这个循环
        for (Map.Entry<Integer, BigDecimal> entry : mapPrincipal.entrySet()) {

            Integer currentPeriod = entry.getKey();//获取期数每一期（1，2，3）
            //根据还款期数获取还款计划的id，lendReturnMap还款集合中传的map集合，键为期数的每一期，值为每一期对应的还款计划id
            //，就可以把回款计划的外键(还款计划)设置到回款计划中了
            //如：lendReturnMap{1:10,2:11,3:12}
            Long lendReturnId = lendReturnMap.get(currentPeriod);//每一月对应的还款id

            //创建回款计划对象
            LendItemReturn lendItemReturn = new LendItemReturn();
            lendItemReturn.setLendReturnId(lendReturnId);//设置标的还款计划id
            lendItemReturn.setLendItemId(lendItemId);//投资记录表id
            lendItemReturn.setLendId(lend.getId());//设置标的id
            lendItemReturn.setInvestUserId(lendItem.getInvestUserId());//设置出借用户id
            lendItemReturn.setInvestAmount(lendItem.getInvestAmount());//设置出借金额
            lendItemReturn.setCurrentPeriod(currentPeriod);//设置期限
            lendItemReturn.setLendYearRate(yearRate);//设置年利率
            lendItemReturn.setReturnMethod(lend.getReturnMethod());//设置还款方式
            //最后一次本金计算 currentPeriod.intValue()：循环的期数 = 投资期数 就是最后一期
            if(lendItemReturnList.size() > 0 && currentPeriod.intValue() == totalMonth.intValue()){
                //最后一期本金 = 本金 - 前几次之和
                BigDecimal sumPrincipal = lendItemReturnList.stream()
                        .map(LendItemReturn::getPrincipal)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                //最后一期应还本金 = 用当前投资人的总投资金额 - 除了最后一期前面期数计算出来的所有的应还本金
                //最后一个月应还本金 = 当前投资人投资金额 - 前几个月应还本金之和
                BigDecimal lastPrincipal = lendItem.getInvestAmount().subtract(sumPrincipal);
                lendItemReturn.setPrincipal(lastPrincipal);//设置最后一期应还本金

                //最后一期利息 = 利息 - 前几次之和
                BigDecimal sumInterest = lendItemReturnList.stream()
                        .map(LendItemReturn::getInterest)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                //最后一期应还利息 = 用当前投资人的总投资金额 - 除了最后一期前面期数计算出来的所有的应还本金
                //最后一个月应还利息 = 当前投资人投资预期收益（总利息） - 前几个月应还利息之和
                BigDecimal lastInterest = lendItem.getExpectAmount().subtract(sumInterest);
                lendItemReturn.setInterest(lastInterest);//设置最后一期应还利息
            }else {
                //设置每一期的的本金和利息，通过每一期的期限获取到本金和利息，最后一个月在if判断里面设置的
                lendItemReturn.setPrincipal(mapPrincipal.get(currentPeriod));//设置除最后一期的应还本金
                lendItemReturn.setInterest(mapInterest.get(currentPeriod));//设置除最后一期的应还利息
            }

            //设置每一期应还本息，本金+利息
            lendItemReturn.setTotal(lendItemReturn.getPrincipal().add(lendItemReturn.getInterest()));
            lendItemReturn.setFee(new BigDecimal("0"));//设置手续费，在放款时借款用户已经交过了
            //设置还款时指定的还款日期，借款日期 + 当前期限，如：当前期限为1，借款日期为8月1日，第一期还款日期为9月1号
            lendItemReturn.setReturnDate(lend.getLendStartDate().plusMonths(currentPeriod));
            //是否逾期，默认未逾期
            lendItemReturn.setOverdue(false);
            lendItemReturn.setStatus(0);

            lendItemReturnList.add(lendItemReturn);

        }

        //批量插入到回款计划表
        lendItemReturnService.saveBatch(lendItemReturnList);

        return lendItemReturnList;

    }




}
