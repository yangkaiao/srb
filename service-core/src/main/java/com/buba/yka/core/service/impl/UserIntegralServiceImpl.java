package com.buba.yka.core.service.impl;

import com.buba.yka.core.mapper.UserIntegralMapper;
import com.buba.yka.core.pojo.entity.UserIntegral;
import com.buba.yka.core.service.UserIntegralService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户积分记录表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class UserIntegralServiceImpl extends ServiceImpl<UserIntegralMapper, UserIntegral> implements UserIntegralService {

}
