package com.buba.yka.core.pojo.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 数据字典excel导入导出。表格中对应的字段，需要用到
 */

@Data
public class ExcelDictDTO {
    //@ExcelProperty：写出时生成excel表格的标题，读入是excel标题根据注解value进行匹配

    @ExcelProperty("id")
    private Long id;

    @ExcelProperty("上级id")
    private Long parentId;

    @ExcelProperty("名称")
    private String name;

    @ExcelProperty("值")
    private Integer value;

    @ExcelProperty("编码")
    private String dictCode;
}