package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.LendItemReturn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 标的出借回款记录表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface LendItemReturnMapper extends BaseMapper<LendItemReturn> {

}
