package com.buba.yka.core.controller.admin;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.result.R;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.core.pojo.entity.IntegralGrade;
import com.buba.yka.core.service.IntegralGradeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 积分等级表 前端控制器，admin后台管理系统的接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Slf4j
@Api(tags = "积分等级管理")
//@CrossOrigin//解决跨域问题，笔记在mvc ajax中
@RestController
@RequestMapping("/admin/core/integralGrade")
public class AdminIntegralGradeController {
    @Resource
    private IntegralGradeService integralGradeService;

    /**
     * 积分等级列表接口
     * @return
     */
    @ApiOperation("积分等级列表")
    @GetMapping("/list")
    public R listAll(){

        log.info("hi i'm helen");
        log.warn("warning!!!");
        log.error("it's a error");

        List<IntegralGrade> list = integralGradeService.list();
        return R.ok().data("list", list).message("获取消息成功");
    }

    @ApiOperation("根据id获取积分等级")
    @GetMapping("/get/{id}")
    public R getById(
            @ApiParam(value = "数据id", required = true, example = "1")
            @PathVariable Long id
    ){

        //只返回给前端这四个字段，因为配置了com.buba.yka.base.config.LocalDateTimeSerializerConfig类
        LambdaQueryWrapper<IntegralGrade> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(IntegralGrade::getId,id)
                .select(IntegralGrade::getId,IntegralGrade::getBorrowAmount,IntegralGrade::getIntegralStart,IntegralGrade::getIntegralEnd);

        IntegralGrade integralGrade = integralGradeService.getOne(queryWrapper);

        if(integralGrade != null){
            return R.ok().data("record", integralGrade);
        }else{
            return R.error().message("数据不存在");
        }
    }


    @ApiOperation("新增积分等级")
    @PostMapping("/save")
    public R save(
            //参数备注
            @ApiParam(value = "积分等级对象", required = true)
            @RequestBody IntegralGrade integralGrade){

//        //如果借款额度为空就手动抛出一个自定义的异常！，抛完异常会被UnifiedExceptionHandler捕获
//        if(integralGrade.getBorrowAmount() == null){
//            //BORROW_AMOUNT_NULL_ERROR(-201, "借款额度不能为空"),如果要抛其它异常就在枚举类添加枚举就行
//            throw new BusinessException(ResponseEnum.BORROW_AMOUNT_NULL_ERROR);
//        }

        //以优雅的 Assert(断言) 方式来校验业务的异常情况，消除 if else
        //如果借款额度为空就手动抛出一个自定义的异常！，抛完异常会被UnifiedExceptionHandler捕获
        Assert.notNull(integralGrade.getBorrowAmount(),ResponseEnum.BORROW_AMOUNT_NULL_ERROR);

        boolean result = integralGradeService.save(integralGrade);
        if (result) {
            return R.ok().message("保存成功");
        } else {
            return R.error().message("保存失败");
        }
    }

    @ApiOperation("更新积分等级")
    @PutMapping("/update")
    public R updateById(
            @ApiParam(value = "积分等级对象", required = true)
            @RequestBody IntegralGrade integralGrade){
        System.out.println("进来了");
        boolean result = integralGradeService.updateById(integralGrade);
        if(result){
            return R.ok().message("修改成功");
        }else{
            return R.error().message("修改失败");
        }
    }


    /**
     * 逻辑删除积分数据接口
     * @param id 积分id
     * @return
     */
    @ApiOperation(value = "根据id删除积分等级", notes="逻辑删除")
    @DeleteMapping("/remove/{id}")
    public R removeById(
            @ApiParam(value = "数据id", required = true, example = "1")
            @PathVariable Long id){
        boolean result = integralGradeService.removeById(id);
        if(result){
            //return R.setResult(ResponseEnum.UPLOAD_ERROR);
            return R.ok().message("删除成功");
        }else{
            return R.error().message("删除失败");
        }
    }


}

