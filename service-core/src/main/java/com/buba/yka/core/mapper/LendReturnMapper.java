package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.LendReturn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 还款记录表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface LendReturnMapper extends BaseMapper<LendReturn> {

}
