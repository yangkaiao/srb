package com.buba.yka.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.mapper.*;
import com.buba.yka.core.pojo.entity.*;
import com.buba.yka.core.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的出借回款记录表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class LendItemReturnServiceImpl extends ServiceImpl<LendItemReturnMapper, LendItemReturn> implements LendItemReturnService {

    @Resource
    private LendReturnMapper lendReturnMapper;

    @Resource
    private LendMapper lendMapper;

    @Resource
    private LendItemMapper lendItemMapper;

    @Resource
    private UserBindService userBindService;

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private DictService dictService;

    /**
     * 网站端标的详情中展示回款计划，获取当前标的的所有投资人回款计划，只有标的投资人能看见
     * @param lendId 标的id
     * @param userId 用户id
     * @return
     */
    @Override
    public List<LendItemReturn> selectByLendId(Long lendId, Long userId) {
        LambdaQueryWrapper<LendItemReturn> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(LendItemReturn::getLendId, lendId)
                .eq(LendItemReturn::getInvestUserId, userId)
                .orderByAsc(LendItemReturn::getCurrentPeriod);
        return baseMapper.selectList(queryWrapper);
    }


    /**
     * 通过还款计划的id，找到对应的回款计划数据，组装data参数中的需要的List<Map>集合
     * @param lendReturnId 还款计划id
     * @return
     */
    @Override
    public List<Map<String, Object>> addReturnDetail(Long lendReturnId) {


        //获取还款计划记录
        LendReturn lendReturn = lendReturnMapper.selectById(lendReturnId);

        //获取当前标的
        Lend lend = lendMapper.selectById(lendReturn.getLendId());

        List<Map<String, Object>> lendItemReturnDetailList = new ArrayList<>();

        //通过还款计划id获取回款计划列表
        List<LendItemReturn> lendItemReturnList = this.selectLendItemReturnList(lendReturnId);
        for (LendItemReturn lendItemReturn : lendItemReturnList) {

            //获取投资人信息
            LendItem lendItem = lendItemMapper.selectById(lendItemReturn.getLendItemId());

            //获取投资人的bind_code
            String bindCodeByUserId = userBindService.getBindCodeByUserId(lendItem.getInvestUserId());
            //data参数需要的List<Map>参数
            //agent_project_code：还款项目（标的）编号。
            //ote_bill_no：投资单号
            //to_bind_code：收款人（投资人）
            //transit_amt：还款金额（投资人应收多少钱） (必须等于base_amt+benifit_amt), 最多支持小数点后2位
            //base_amt：还款本金。最多小数点后2位
            //benifit_amt：还款利息(如果base_amt、benifit_amt都大于0，则benifit_amt  不能大于base_amt的50%，且该投资单所有还款利息不得超过投资金额的50%),最多小数点后2位。base_amt、benifit_amt至少有一项必须大于0
            //fee_amt：商户手续费。最多小数点后2位
            HashMap<String, Object> map = new HashMap<>();
            map.put("agentProjectCode",lend.getLendNo());//还款项目（标的）编号。
            map.put("voteBillNo",lendItem.getLendItemNo());//投资单号
            map.put("toBindCode",bindCodeByUserId);//投资人bind_code
            //还款金额本息（投资人应收多少钱）
            map.put("transitAmt",lendItemReturn.getTotal());
            //还款本金
            map.put("baseAmt", lendItemReturn.getPrincipal());
            //还款利息
            map.put("benifitAmt", lendItemReturn.getInterest());
            //商户手续费
            map.put("feeAmt", new BigDecimal("0"));

            lendItemReturnDetailList.add(map);
        }

        return lendItemReturnDetailList;
    }


    /**
     * 通过还款计划id获取回款计划列表
     * @param lendReturnId 还款计划id
     * @return
     */
    @Override
    public List<LendItemReturn> selectLendItemReturnList(Long lendReturnId) {
        LambdaQueryWrapper<LendItemReturn> lendItemReturnLambdaQueryWrapper = new LambdaQueryWrapper<>();
        lendItemReturnLambdaQueryWrapper.eq(LendItemReturn::getLendReturnId,lendReturnId);
        List<LendItemReturn> lendItemReturns = baseMapper.selectList(lendItemReturnLambdaQueryWrapper);
        return lendItemReturns;
    }


    /**
     * 在网站端当前用户主页面，可查看的回款计划
     * @param page1
     * @param userId
     * @return
     */
    @Override
    public Page<LendItemReturn> selectLendReturnItem(Page<LendItemReturn> page1, Long userId) {
        LambdaQueryWrapper<LendItemReturn> lendReturnLambdaQueryWrapper = new LambdaQueryWrapper<>();
        lendReturnLambdaQueryWrapper.eq(LendItemReturn::getInvestUserId,userId);

        Page<LendItemReturn> selectPage = baseMapper.selectPage(page1, lendReturnLambdaQueryWrapper);

        selectPage.getRecords().forEach(item ->{
            UserInfo userInfo = userInfoMapper.selectById(item.getInvestUserId());
            Lend lend = lendMapper.selectById(item.getLendId());
            String returnMethod = dictService.getNameByParentDictCodeAndValue("returnMethod", item.getReturnMethod());
            item.getParam().put("userName",userInfo.getName());//投资人名称
            item.getParam().put("returnMethod",returnMethod);
            item.getParam().put("lendName",lend.getTitle());//标的名称
        });

        return selectPage;
    }


}
