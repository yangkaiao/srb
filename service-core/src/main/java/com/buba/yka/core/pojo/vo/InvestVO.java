package com.buba.yka.core.pojo.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 组装表单，接收用户界面投资时传的参数
 */
@Data
@ApiModel(description = "投标信息")
public class InvestVO {

    //标的id
    private Long lendId;

    //投标金额
    private String investAmount;

    //用户id
    private Long investUserId;

    //用户姓名
    private String investName;
}