package com.buba.yka.core.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.pojo.entity.LendItem;
import com.baomidou.mybatisplus.extension.service.IService;
import com.buba.yka.core.pojo.vo.InvestVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 标的出借记录表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface LendItemService extends IService<LendItem> {

    //投资人对标的进行投资
    String commitInvest(InvestVO investVO);

    //异步回调
    void notify(Map<String, Object> paramMap);

    //通过标的id和状态为1已支付的查询lendItem表信息
    List<LendItem> selectByLendId(Long lendId, Integer status);

    //在标的详情展示投资记录
    List<LendItem> selectByLendId(Long lendId);

    //在网站端当前用户主页面，可查看的投资列表
    Page<LendItem> selectListPage(Page<LendItem> pageParam, Long userId);
}
