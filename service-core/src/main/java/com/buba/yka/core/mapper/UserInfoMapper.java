package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户基本信息 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
