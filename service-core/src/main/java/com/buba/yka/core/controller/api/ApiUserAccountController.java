package com.buba.yka.core.controller.api;


import com.alibaba.fastjson.JSON;
import com.buba.yka.base.utils.JwtUtils;
import com.buba.yka.common.result.R;
import com.buba.yka.core.hfb.RequestHelper;
import com.buba.yka.core.service.UserAccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 用户账户 前端控制器，账户充值
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "投资人账户")
@RestController
@RequestMapping("/api/core/userAccount")
@Slf4j
public class ApiUserAccountController {

    @Resource
    private UserAccountService userAccountService;


    /**
     * 账户充值，尚融宝对汇付宝发送请求，携带着固定参数（汇付宝要求的）
     *         //之前用户账号绑定时，汇付宝向尚融宝发送的异步请求参数中有绑定协议号（bind_code），添加到user_info表中和user_bind表中了。
     *         //所以获取当前登录的用户的bind_code，发送给汇付宝与汇付宝的bind_code进行统一绑定，
     *         //汇付宝通过bind_code获取到user_bind表中的用户绑定信息，在对指定的bind_code的用户进行充值
     *         汇付包user_account表更新user_account记录中的amount的值
     * 与账户绑定流程一样（ApiUserBindController）
     * @param chargeAmt 充值金额
     * @param request 获取token，当前登录用户id
     * @return 给前端返回一个动态html，表单自动提交，生成一个动态表单的字符串，表单携带着固定参数：表单自动提交对汇付宝发送请求
     */
    @ApiOperation("账户充值")
    @PostMapping("/auth/commitCharge/{chargeAmt}")
    public R commitCharge(
            @ApiParam(value = "充值金额", required = true)
            @PathVariable BigDecimal chargeAmt, HttpServletRequest request) {

        String token = request.getHeader("token");
        //从header中获取token，并对token进行校验，确保用户已登录，并从token中提取userId
        Long userId = JwtUtils.getUserId(token);//从token中提取userId，并进行了校验

        String formStr = userAccountService.commitCharge(chargeAmt, userId);

        //生成一个动态表单的字符串，给前端返回一个html，表单自动提交
        return R.ok().data("formStr", formStr);
    }


    /**
     * 异步回调，与汇付宝数据同步，汇付宝向尚融宝发送请求，并携带固定参数
     * 目的：
     * （1）尚融宝账户金额更改（user_account）
     * （2）尚融宝添加交易流水（trans_flow）
     * 汇付宝充值成功后异步通知到尚融宝携带固定参数有bind_code,尚融宝在通过bind_code查询user_info表的用户id，在通过用户id更新user_account表
     * @param request 获取汇付宝向尚融宝发送的参数
     * @return 尚融宝对汇付包返回success，返回success汇付包就不会发送重试了（不会报错了）成功必须返回success
     */
    @ApiOperation(value = "用户充值异步回调")
    @PostMapping("/notify")
    public String notify(HttpServletRequest request) {
        //汇付包向尚融宝发起回调请求时携带的参数
        Map<String, Object> paramMap = RequestHelper.switchMap(request.getParameterMap());
        log.info("用户充值异步回调接收的参数如下：" + JSON.toJSONString(paramMap));
        //用户充值异步回调接收的参数如下：
        // {"chargeAmt":"10", 充值金额
        // "hyFee":"0", 汇付宝收取商户的管理费
        // "mchFee":"0", 商户收取的用户的手续费。
        // "resultCode":"0001",结果编码。0001=绑定成功且通过实名认证、E105=绑定失败、U999=未知错误
        // "sign":"b9f7ad809cc6a11a81a489976b90b318",签名。
        // "bindCode":"448d63f136bb4b46a24026c447574a9b",充值人绑定协议号。
        // "agentBillNo":"20230807083959801", 商户充值订单号流水号
        // "resultMsg":"充值成功", 充值结果描述
        // "timestamp":"1691368812607"}通知时间。从1970-01-01 00:00:00算起的毫秒数。绑定错误不返回。

        //校验签名
        if(RequestHelper.isSignEquals(paramMap)) {
            //充值成功交易，因为001是硬编码死数据，按正常来说这个交易结果是汇付宝与银行对接的，银行给出交易结果，所有没有办法实现只能硬编码成功
            if("0001".equals(paramMap.get("resultCode"))) {

                //（1）尚融宝账户金额更改（user_account）（2）尚融宝添加交易流水（trans_flow）
                return userAccountService.notify(paramMap);

            }
            //不会走这里，因为按正常来说这个交易结果是汇付宝与银行对接的，银行给出交易结果，所有没有办法实现只能硬编码成功
            else {
                log.info("用户充值异步回调充值失败：" + JSON.toJSONString(paramMap));
                return "success";
            }
        } else {
            log.info("用户充值异步回调签名错误：" + JSON.toJSONString(paramMap));
            return "fail";
        }
    }


    /**
     * 通过用户id查询当前投资人账户余额
     * @param request 当前用户
     * @return
     */
    @ApiOperation("查询账户余额")
    @GetMapping("/auth/getAccount")
    public R getAccount(HttpServletRequest request){
        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        BigDecimal account = userAccountService.getAccount(userId);
        return R.ok().data("account", account);
    }

    /**
     * 用户提现，提现到银行（因为没有银行所以，所以是虚拟的），尚融宝调用汇付宝接口并携带固定参数
     * 汇付宝业务：更改user_account表，当前用户账户金额减去提现金额（amount字段）
     * @param fetchAmt 提现金额
     * @param request 当前用户
     * @return
     */
    @ApiOperation("用户提现")
    @PostMapping("/auth/commitWithdraw/{fetchAmt}")
    public R commitWithdraw(
            @ApiParam(value = "金额", required = true)
            @PathVariable BigDecimal fetchAmt, HttpServletRequest request) {

        String token = request.getHeader("token");
        Long userId = JwtUtils.getUserId(token);
        String formStr = userAccountService.commitWithdraw(fetchAmt, userId);
        return R.ok().data("formStr", formStr);
    }

    /**
     * 用户提现异步回调，汇付宝请求尚融宝接口并携带参数，把数据同步到尚融宝
     * 尚融宝业务：更改user_account表，当前用户账户金额减去提现金额（amount字段），增加交易流水(trans_flow表)
     * @param request 携带的参数
     * @return
     */
    @ApiOperation("用户提现异步回调")
    @PostMapping("/notifyWithdraw")
    public String notifyWithdraw(HttpServletRequest request) {
        Map<String, Object> paramMap = RequestHelper.switchMap(request.getParameterMap());
        log.info("提现异步回调：" + JSON.toJSONString(paramMap));

        //校验签名
        if(RequestHelper.isSignEquals(paramMap)) {
            //提现成功交易
            if("0001".equals(paramMap.get("resultCode"))) {
                userAccountService.notifyWithdraw(paramMap);
            } else {
                log.info("提现异步回调充值失败：" + JSON.toJSONString(paramMap));
                return "fail";
            }
        } else {
            log.info("提现异步回调签名错误：" + JSON.toJSONString(paramMap));
            return "fail";
        }
        return "success";
    }

}

