package com.buba.yka.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.buba.yka.core.enums.TransTypeEnum;
import com.buba.yka.core.mapper.TransFlowMapper;
import com.buba.yka.core.mapper.UserInfoMapper;
import com.buba.yka.core.pojo.bo.TransFlowBO;
import com.buba.yka.core.pojo.entity.TransFlow;
import com.buba.yka.core.pojo.entity.UserInfo;
import com.buba.yka.core.service.TransFlowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 交易流水表 服务实现类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Service
public class TransFlowServiceImpl extends ServiceImpl<TransFlowMapper, TransFlow> implements TransFlowService {

    @Resource
    private UserInfoMapper userInfoMapper;

    /**
     * 尚融宝添加交易流水（trans_flow）
     * @param transFlowBO
     */
    @Override
    public void saveTransFlow(TransFlowBO transFlowBO) {

        //通过bind_code获取用户对象
        LambdaQueryWrapper<UserInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserInfo::getBindCode,transFlowBO.getBindCode());
        UserInfo userInfo = userInfoMapper.selectOne(queryWrapper);

        TransFlow transFlow = new TransFlow();
        transFlow.setUserId(userInfo.getId());//设置用户id
        transFlow.setUserName(userInfo.getName());//设置用户名称
        transFlow.setTransAmount(transFlowBO.getAmount());//设置金额
        transFlow.setTransNo(transFlowBO.getAgentBillNo());//设置交易单号，流水号
        transFlow.setTransType(transFlowBO.getTransTypeEnum().getTransType());//设置交易类型编号（1：充值 2：提现 3：投标 4：投资回款 ...）
        transFlow.setTransTypeName(transFlowBO.getTransTypeEnum().getTransTypeName());//设置交易类型名称（1：充值 2：提现 3：投标 4：投资回款 ...）
        transFlow.setMemo(transFlowBO.getMemo());//交易流水描述备注
        baseMapper.insert(transFlow);
    }


    /**
     * 判断流水号是否存在，解决当回调重试时，金额和流水会重复增加
     * @param agentBillNo 流水号
     * @return
     */
    @Override
    public boolean isSaveTransFlow(String agentBillNo) {
        LambdaQueryWrapper<TransFlow> transFlowLambdaQueryWrapper = new LambdaQueryWrapper<>();
        transFlowLambdaQueryWrapper.eq(TransFlow::getTransNo,agentBillNo);
        Long count = baseMapper.selectCount(transFlowLambdaQueryWrapper);
        return count > 0;//如果大于0则存在，返回true
    }


    /**
     * 查询当前用户的资金流水
     * @param userId
     * @return
     */
    @Override
    public Page<TransFlow> selectByUserId(Page<TransFlow> page,Long userId) {

        LambdaQueryWrapper<TransFlow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(TransFlow::getUserId, userId)
                .orderByDesc(TransFlow::getId);
        Page<TransFlow> transFlowPage = baseMapper.selectPage(page, queryWrapper);
        return transFlowPage;
    }
}
