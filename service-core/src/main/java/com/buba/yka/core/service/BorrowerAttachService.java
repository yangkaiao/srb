package com.buba.yka.core.service;

import com.buba.yka.core.pojo.entity.BorrowerAttach;
import com.baomidou.mybatisplus.extension.service.IService;
import com.buba.yka.core.pojo.vo.BorrowerAttachVO;

import java.util.List;

/**
 * <p>
 * 借款人上传资源表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface BorrowerAttachService extends IService<BorrowerAttach> {

    //通过借款人id查询借款人附件图片
    List<BorrowerAttachVO> selectBorrowerAttachVOList(Long id);
}
