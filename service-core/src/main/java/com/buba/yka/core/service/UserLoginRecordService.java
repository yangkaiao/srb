package com.buba.yka.core.service;

import com.buba.yka.core.pojo.entity.UserLoginRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户登录记录表 服务类
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface UserLoginRecordService extends IService<UserLoginRecord> {

    //查询该用户的登录日志列表
    List<UserLoginRecord> listTop50(Long userId);
}
