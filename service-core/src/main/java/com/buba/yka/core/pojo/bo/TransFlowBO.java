package com.buba.yka.core.pojo.bo;

import com.buba.yka.core.enums.TransTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 作用：业务对象，作用在controller层和service层和mapper称之间的调用，封装一个类，就不需要一个一个传了
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransFlowBO {

    private String agentBillNo;//交易单号，流水号
    private String bindCode;//绑定协议号
    private BigDecimal amount;//充值金额
    private TransTypeEnum transTypeEnum;//交易类型
    private String memo;//描述
}