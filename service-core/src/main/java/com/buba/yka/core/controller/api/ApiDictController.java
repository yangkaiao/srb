package com.buba.yka.core.controller.api;


import com.buba.yka.common.result.R;
import com.buba.yka.core.pojo.entity.Dict;
import com.buba.yka.core.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 数据字典 前端控制器
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
@Api(tags = "用户界面需要的数据字典")
@RestController
@RequestMapping("/api/core/dict")
@Slf4j
public class ApiDictController {

    @Resource
    private DictService dictService;

    @ApiOperation("根据dictCode编码获取下级节点")
    @GetMapping("/findByDictCode/{dictCode}")
    public R findByDictCode(
            @ApiParam(value = "节点编码", required = true)
            @PathVariable String dictCode) {

        //根据dictCode编码获取下级节点
        List<Dict> list = dictService.findByDictCode(dictCode);

        return R.ok().data("dictList", list);
    }


}

