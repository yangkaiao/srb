package com.buba.yka.core.mapper;

import com.buba.yka.core.pojo.entity.TransFlow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 交易流水表 Mapper 接口
 * </p>
 *
 * @author yka
 * @since 2023-07-13
 */
public interface TransFlowMapper extends BaseMapper<TransFlow> {

}
