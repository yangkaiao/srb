package com.buba.yka.rabbitutil.constant;

/**
 * 发送信息和接收消息的关键因素，发送消息的配置的交换机和路由，接收消息时必须接收同一交换机和路径下的消息
 */
public class MQConst {

    public static final String EXCHANGE_TOPIC_SMS = "exchange.topic.sms";//交换机
    public static final String ROUTING_SMS_ITEM = "routing.sms.item";//路由
    public static final String QUEUE_SMS_ITEM  = "queue.sms.item";//消息队列
}