package com.buba.yka.sms.service;

import java.util.Map;

public interface SmsService {

    //发送短信
    void send(String mobile, Map<String,Object> param);
}