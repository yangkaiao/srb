package com.buba.yka.sms.service.impl;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.tea.TeaException;

import com.aliyun.teautil.models.RuntimeOptions;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.buba.yka.common.exception.Assert;
import com.buba.yka.common.exception.BusinessException;
import com.buba.yka.common.result.ResponseEnum;
import com.buba.yka.sms.service.SmsService;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Auther: Microhoo
 * @Date: 2023.07.20 - 07 - 20 - 14:13
 * @Description: com.atsufu.sms.SmsService.impl
 * @version: 1.0
 */
@Slf4j
@Service
public class SmsServiceImpl implements SmsService {


    @Value("${aliyun.sms.key-id}")
    private String keyId;
    @Value("${aliyun.sms.key-secret}")
    private String keySecret;
    @Value("${aliyun.sms.sign-name}")
    private String signName;
    @Value("${aliyun.sms.template-code}")
    private String templateCode;

    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                // 必填，您的 AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 必填，您的 AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

    @Override
    public void send(String mobile, Map<String, Object> param) {
        // 工程代码泄露可能会导致AccessKey泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议使用更安全的 STS 方式，更多鉴权访问方式请参见：https://help.aliyun.com/document_detail/378657.html
        Client client = null;
        try {
            client = createClient(keyId, keySecret);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 将传过来的map转成Json格式
        Gson gson = new Gson();
        String json = gson.toJson(param);

        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setSignName(signName)//签名名称，不能修改，这是在阿里云网站上申请过的签名，就是短信的标题
                .setTemplateCode(templateCode)//模板CODE,不能修改，这是在阿里云网站上申请过的模板
                .setPhoneNumbers(mobile)//要发送验证码的手机号
                .setTemplateParam(json);//验证码,随机生产4位验证码
        RuntimeOptions runtime = new RuntimeOptions();
        try {
            // 使用客户端对象携带请求参数想远程阿里云服务器发起远程调用，并得到响应结果
            SendSmsResponse response = client.sendSmsWithOptions(sendSmsRequest, runtime);


            // 与阿里云连接上了，获取响应结果
            String code = response.getBody().getCode();
            String message = response.getBody().getMessage();
            log.info("阿里云短信发送响应结果：");
            log.info("code：" + code);
            log.info("message：" + message);

            //业务失败处理
            //ALIYUN_SMS_LIMIT_CONTROL_ERROR(-502, "短信发送过于频繁"),//业务限流，code为isv.BUSINESS_LIMIT_CONTROL时抛异常
            Assert.notEquals("isv.BUSINESS_LIMIT_CONTROL", code, ResponseEnum.ALIYUN_SMS_LIMIT_CONTROL_ERROR);
            //ALIYUN_SMS_ERROR(-503, "短信发送失败"),//其他失败，如果code不是‘ok’则抛异常
            Assert.equals("OK", code, ResponseEnum.ALIYUN_SMS_ERROR);

        }
//        ServerException通常表示服务器处理请求时发生了错误。具体可能的原因包括:
//        - 服务器代码存在bug,在处理请求逻辑时抛出了 RuntimeException。
//        - 服务器依赖的外部系统或资源不可用,导致请求失败。
//        - 服务器过载或繁忙,无法处理请求。
//        - 网络通信故障,请求无法到达服务器。
//        - 请求参数或数据格式错误,服务器无法解析。
//        - 请求需要的资源如文件、数据库记录不存在。
//        - 请求权限验证失败。
//        - 其他服务器内部错误。
        catch (ServerException e) {
            log.error("阿里云短信发送SDK调用失败服务器错误：");
            log.error("ErrorCode=" + e.getErrCode());
            log.error("ErrorMessage=" + e.getErrMsg());
            throw new BusinessException(ResponseEnum.ALIYUN_SMS_ERROR , e);
        }
//        ClientException 的具体场景包括:
//        - 客户端请求参数错误,如验证失败、非法参数等
//        - 客户端请求频率过快被限制
//        - 客户端Token/签名过期失效
//        - 客户端版本太旧,接口升级不兼容导致
//        - 客户端网络问题,请求超时等
        catch (ClientException e) {
            log.error("阿里云短信发送SDK调用失败客户端错误：");
            log.error("ErrorCode=" + e.getErrCode());
            log.error("ErrorMessage=" + e.getErrMsg());
            throw new BusinessException(ResponseEnum.ALIYUN_SMS_ERROR , e);
        } catch (Exception e) {
            //通信失败的处理，说明没有与阿里云连接上，id和密码错了或者模板签名不对
            log.error("通信失败的处理，连接失败");
            throw new BusinessException(ResponseEnum.ALIYUN_RESPONSE_FAIL , e);
        }
    }
}
